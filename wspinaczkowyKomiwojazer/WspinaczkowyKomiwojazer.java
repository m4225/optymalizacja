package wspinaczkowyKomiwojazer;

public class WspinaczkowyKomiwojazer {

    public static boolean IF_DEBUG = false;

    static Osobnik generujNowegOsobnik(Osobnik stary, int[][] odleglosci) {
        Osobnik wynik = new Osobnik(stary.wymiar);

        for (int i = 0; i < stary.wymiar; i++) {
            wynik.genes[i] = stary.genes[i];
        }

        int geneIdx = (int) (Math.random() * 1000) % stary.wymiar;

        var lastIdx = stary.wymiar - 1;
        for (int i = lastIdx; i > 0; i--) {
            wynik.kosz[lastIdx - i] = i;
        }

        wynik.genes[geneIdx] = (int) (Math.random() * 1000) % (16 - geneIdx);
        wynik.UstalTrase();
        wynik.PoliczDopasowanie(odleglosci);

        return wynik;
    }

    static Osobnik wybierz(Osobnik stary, Osobnik nowy) {
        if (stary.fitness < nowy.fitness) {
            return stary;
        }

        if (IF_DEBUG) {
            System.out.println("zmieniono");
        }
        return nowy;
    }

    public static void main(String[] args) {
        var k = new Komiwojazer();
        try {
            k.Wczytaj("miasta.txt");
        } catch (Exception e) {
            System.out.println(e);
        }

        k.Wyswietl();

        var o = new Osobnik(k.wymiar);
        o.Inicjalizuj();
        o.PoliczDopasowanie(k.odleglosci);

        o.WyswietlGeny();
        o.WyswietlTrase();
        o.WyswietlDopasowanie();

        for (int i = 0; o.fitness>2849.0; i++) {
            System.out.println("ITERACJA " + i);
            Osobnik nowyOs = generujNowegOsobnik(o, k.odleglosci);
            if (IF_DEBUG) {
                System.out.print("Nowy osobnik: ");
            }
            nowyOs.WyswietlGeny();

            o = wybierz(o, nowyOs);

            if (IF_DEBUG)
                System.out.print("Wybrany osobnik: ");

            o.WyswietlGeny();

            o.WyswietlDopasowanie();
        }
    }
}
