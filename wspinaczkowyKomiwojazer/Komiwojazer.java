package wspinaczkowyKomiwojazer;

import java.io.File;
import java.util.*;

public class Komiwojazer {
    public int[][] odleglosci;
    public int wymiar;

    void Wczytaj(String nazwa) throws Exception {
        File f = new File(nazwa);
        Scanner in = new Scanner(f);

        List<Integer> odl = new ArrayList<Integer>();
        while (in.hasNextInt()) {
            var punkt = in.nextInt();
            odl.add(punkt);
        }

        in.close();
        wymiar = (int) Math.sqrt(odl.size());

        odleglosci = new int[wymiar][wymiar];
        for (int i = 0; i < wymiar; i++)
            for (int j = 0; j < wymiar; j++) {
                var idx = i * wymiar + j;
                odleglosci[i][j] = odl.get(idx);
            }
    }

    void Wyswietl() {
        System.out.println("Dane:");
        for (int i = 0; i < wymiar; i++) {
            for (int j = 0; j < wymiar; j++) {
                System.out.print(odleglosci[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
