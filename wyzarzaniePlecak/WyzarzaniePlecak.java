package wyzarzaniePlecak;

public class WyzarzaniePlecak {
    public static boolean IF_DEBUG = true;

    //generuje nowego osobnika zmieniając jeden losowy gen po czym ustawia kolejność elementów w plecaku i liczy dopasowanie
    static Osobnik generujNowegOsobnik(Osobnik stary, double[] ceny, double[] wagi, double maxWaga, int wymiar) {
        Osobnik wynik = new Osobnik(stary.wymiar, maxWaga);

        for (int i = 0; i < stary.wymiar; i++) {
            wynik.genes[i] = stary.genes[i];
        }

        int geneIdx = (int) (Math.random() * 1000) % stary.wymiar;

        var lastIdx = stary.wymiar - 1;
        for (int i = lastIdx; i > 0; i--) {
            wynik.kosz[lastIdx - i] = i;
        }

        wynik.genes[geneIdx] = (int) (Math.random() * 1000) % (wymiar - geneIdx);
        wynik.UstalPlecak();
        wynik.PoliczDopasowanie(ceny, wagi);

        return wynik;
    }

    //funkcja licząca temperaturę
    static double temperatura(double tempP, double tempK, double maxZmian, int n) {
        return tempP - Math.log(n * (tempP - tempK) / (maxZmian));
    }

    // funkcja wybierająca osobnika 
    // jeżeli nowy osobnik jest lepszy niż stary to nowy jest wybrany
    //lub jeżeli stary jest lepszy nowy gorszy osobnik jest wybierany z prawdopodobieństwem p liczonym na bazie temperatury
    static Osobnik wybierz(Osobnik nowy, Osobnik stary, double temp, int n) {
        System.out.println("iteracja " + n);

        if (nowy.fitness > stary.fitness) {
            System.out.println("zamieniam ");
            return nowy;
        } else {
            var roznica = stary.fitness - nowy.fitness;
            var p = Math.exp(roznica / temp);
            if ((Math.random()) < p) {
                System.out.println("zamieniam na gorszy z prawdopodobienstwem: " + p + " i roznica: " + roznica);
                return nowy;
            }
        }

        return stary;
    }

    public static void main(String[] args) {
        double tempP = 801;
        double tempK = 1;
        double maxZmian = 801;
        double maxWaga = 64.0;
        double temp = tempP;
        var k = new Plecakowy();
        try {
            k.Wczytaj("przedmioty.txt");
        } catch (Exception e) {
            System.out.println(e);
        }

        var o = new Osobnik(k.wymiar, maxWaga);
        o.Inicjalizuj();
        o.PoliczDopasowanie(k.ceny, k.wagi);

        o.WyswietlGeny();
        o.WyswietlPlecak(k.ceny, k.wagi);
        o.WyswietlDopasowanie();

        var najlepszy = o;

        for (int i = 0; i < 800; i++) {
            var nowy = generujNowegOsobnik(o, k.ceny, k.wagi, maxWaga, k.wymiar);
            temp = temperatura(temp, tempK, maxZmian, i + 1);
            o = wybierz(nowy, o, temp, i);
            if (o.fitness > najlepszy.fitness) {
                najlepszy = o;
            }
            o.WyswietlDopasowanie();
        }

        System.out.println("\nOstatni:");
        o.WyswietlGeny();
        o.WyswietlPlecak(k.ceny, k.wagi);
        najlepszy.WyswietlDopasowanie();

        System.out.println("\nNajlepszy:");
        najlepszy.WyswietlGeny();
        najlepszy.WyswietlPlecak(k.ceny, k.wagi);
        najlepszy.WyswietlDopasowanie();
    }
}
