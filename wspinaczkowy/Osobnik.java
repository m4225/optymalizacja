package wspinaczkowy;

public class Osobnik {
    double[] genes;
    double fitness;

    void policzFitness() {
        fitness = 1000.0;
        for (double gene : genes) {
            fitness *= Math.sin(gene);
        }

    }
}
