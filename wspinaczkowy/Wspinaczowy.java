package wspinaczkowy;

public class Wspinaczowy {

    static Osobnik generujOsobnika() {
        Osobnik os = new Osobnik();

        double[] genes = new double[5];
        for (int i = 0; i < 5; i++) {
            genes[i] = Math.random() * Math.PI;
            System.out.print(genes[i]);
            System.out.print(" ");
        }

        System.out.println();
        os.genes = genes;

        return os;
    }

    static Osobnik generujNowegOsobnik(Osobnik stary) {
        Osobnik wynik = new Osobnik();
        wynik.genes = new double[5];

        for (int i = 0; i < 5; i++) {
            wynik.genes[i] = stary.genes[i];
        }

        int gene_idx = (int) (Math.random() * 100) % 5;

        double staryGen = wynik.genes[gene_idx];

        wynik.genes[gene_idx] += Math.random() - 0.5;

        if (wynik.genes[gene_idx] < 0) {
            wynik.genes[gene_idx] = 0;
        } else if (wynik.genes[gene_idx] > Math.PI) {
            wynik.genes[gene_idx] = Math.PI;
        }

        System.out.println("Gen: " + gene_idx + " Stary gen: " + staryGen + " Nowy gen: " + wynik.genes[gene_idx]);

        return wynik;
    }

    static void printGeny(Osobnik os) {
        for (int i = 0; i < 5; i++) {
            System.out.print(os.genes[i] + " ");
        }

        System.out.print(" Fitness: " + os.fitness);
    }

    static Osobnik wybierz(Osobnik stary, Osobnik nowy) {
        stary.policzFitness();
        nowy.policzFitness();
        if (stary.fitness > nowy.fitness) {
            return stary;
        }

        System.out.println("zmieniono");
        return nowy;
    }

    public static void main(String[] args) {
        Osobnik os = generujOsobnika();
        os.policzFitness();
        System.out.print("Pierwszy osobnik: ");
        printGeny(os);
        System.out.println();
        for (int i = 0; os.fitness < 999.99; i++) {
            System.out.println("ITERACJA " + i);
            Osobnik nowyOs = generujNowegOsobnik(os);
            nowyOs.policzFitness();
            nowyOs.policzFitness();
            System.out.print("Nowy osobnik: ");
            printGeny(nowyOs);
            System.out.println();

            os = wybierz(os, nowyOs);
            os.policzFitness();
            System.out.print("Wybrany osobnik: ");
            printGeny(os);
            System.out.println();

            System.out.println();
        }
    }
}