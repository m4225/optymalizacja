package wyzarzanieKomiwojazer;

public class WyzarzanieKomiwojazer {
    public static boolean IF_DEBUG = true;

    static Osobnik generujNowegOsobnik(Osobnik stary, int[][] odleglosci) {
        Osobnik wynik = new Osobnik(stary.wymiar);

        for (int i = 0; i < stary.wymiar; i++) {
            wynik.genes[i] = stary.genes[i];
        }

        int geneIdx = (int) (Math.random() * 1000) % stary.wymiar;

        var lastIdx = stary.wymiar - 1;
        for (int i = lastIdx; i > 0; i--) {
            wynik.kosz[lastIdx - i] = i;
        }

        wynik.genes[geneIdx] = (int) (Math.random() * 1000) % (16 - geneIdx);
        wynik.UstalTrase();
        wynik.PoliczDopasowanie(odleglosci);

        return wynik;
    }

    static double temperatura(double tempP, double tempK, double maxZmian, int n) {
        return tempP - Math.log(n * (tempP - tempK) / (maxZmian));
    }

    static Osobnik wybierz(Osobnik nowy, Osobnik stary, double temp, int n) {
        System.out.println("iteracja " + n);

        if (nowy.fitness < stary.fitness) {
            System.out.println("zamieniam ");
            return nowy;
        } else {
            var roznica = stary.fitness - nowy.fitness;
            var p = Math.exp(roznica / temp);
            // System.out.println("Iteracja: " + n + " Temperatura: " + temp + "
            // Prawdopodbienstwo: " + p + " Roznica: "
            // + roznica);
            if ((Math.random()) < p) {
                System.out.println("zamieniam na gorszy z prawdopodobienstwem: " + p + " i roznica: " + roznica);
                return nowy;
            }
        }

        return stary;
    }

    public static void main(String[] args) {
        double tempP = 801;
        double tempK = 1;
        double maxZmian = 801;
        double temp = tempP;
        var k = new Komiwojazer();
        try {
            k.Wczytaj("miasta.txt");
        } catch (Exception e) {
            System.out.println(e);
        }

        k.Wyswietl();

        var o = new Osobnik(k.wymiar);
        o.Inicjalizuj();
        o.PoliczDopasowanie(k.odleglosci);

        o.WyswietlGeny();
        o.WyswietlTrase();
        o.WyswietlDopasowanie();

        var najlepszy = o;

        for (int i = 0; i < 800; i++) {
            var nowy = generujNowegOsobnik(o, k.odleglosci);
            temp = temperatura(temp, tempK, maxZmian, i + 1);
            o = wybierz(nowy, o, temp, i);
            if (o.fitness < najlepszy.fitness) {
                najlepszy = o;
            }
            o.WyswietlDopasowanie();
        }

        System.out.println("\nOstatni:");
        o.WyswietlGeny();
        o.WyswietlTrase();
        najlepszy.WyswietlDopasowanie();

        System.out.println("\nNajlepszy:");
        najlepszy.WyswietlGeny();
        najlepszy.WyswietlTrase();
        najlepszy.WyswietlDopasowanie();
    }
}
