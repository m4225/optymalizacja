package ewolucyjnyKomiwojazer;

public class Osobnik {
    public int wymiar;
    int[] genes, kosz, trasa;
    double fitness, cfitness, rfitenss;

    public Osobnik(int wymiar) {
        super();
        this.wymiar = wymiar;
        genes = new int[wymiar];
        kosz = new int[wymiar];
        trasa = new int[wymiar];
    }

    public void Inicjalizuj() {
        var lastIdx = wymiar - 1;
        for (int i = lastIdx; i > 0; i--) {
            genes[lastIdx - i] = (int) (Math.random() * 1000) % i;
            kosz[lastIdx - i] = i;
        }

        genes[lastIdx] = 0;
        kosz[lastIdx] = 0;
        this.UstalTrase();
    }

    public void UstalTrase() {
        var temp_kosz = new int[kosz.length];
        for (int i = 0; i < kosz.length; i++)
            temp_kosz[i] = kosz[i];
        for (int i = 0; i < wymiar; i++) {
            trasa[i] = kosz[genes[i]];
            usunZKosza(genes[i]);
        }
        for (int i = 0; i < kosz.length; i++)
            kosz[i] = temp_kosz[i];
    }

    void usunZKosza(int poz) {
        for (int i = poz; i < wymiar - 1; i++) {
            kosz[i] = kosz[i + 1];
        }
    }

    public void policzFitness(int[][] odleglosci) {
        int lastIdx = wymiar - 1;
        fitness = odleglosci[trasa[lastIdx]][trasa[0]];
        for (int i = 0; i < lastIdx; i++) {
            fitness += odleglosci[trasa[i]][trasa[i + 1]];
        }
    }

    public void WyswietlTrase() {
        if (!EwolucyjnyKomiwojazer.IF_DEBUG) {
            return;
        }

        System.out.print("Trasa:\t");
        for (int t : trasa) {
            System.out.print(t + " ");
        }
        System.out.println();
    }

    public void WyswietlGeny() {
        if (!EwolucyjnyKomiwojazer.IF_DEBUG) {
            return;
        }

        System.out.print("Geny:\t");
        for (int t : genes) {
            System.out.print(t + " ");
        }
        System.out.println();
    }

    public void WyswietlDopasowanie() {
        System.out.println("Dopasowanie: " + fitness);
    }
}
