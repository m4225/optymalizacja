package ewolucyjnyKomiwojazer;

import java.util.HashMap;
import java.util.Map;

public class EwolucyjnyKomiwojazer {
    public static boolean IF_DEBUG = false;
    public static void main(String[] args) {
        var startPopulacji = 25;
        var koniecPopulacji=200;
        var startMaxGeneracji=100;
        var koniecMaxGeneracji=10000;
        var result = new HashMap<Integer, Map<Integer, Double>>();
        for (int i=startPopulacji; i<=koniecPopulacji; i++){
            var temporary = new HashMap<Integer, Double>();
            for (int j=startMaxGeneracji; j<=koniecMaxGeneracji; j+=10){
                var list = 0.0;
                for (int k=0; k<100;k++)
                    list +=glowna(i, j);

                list=(Double)(list/100.0);
                temporary.put(j, list);
                System.out.println(i+" "+j+" "+list);
            }

            result.put(i, temporary);
        }
    }

    public static int glowna(int wielkoscPopulacji, int maxGeneracji){
        var pKrzyzowania = 0.8;
        var pMutacji = 0.2;

        var k = new Komiwojazer();
        try {
            k.Wczytaj("miasta.txt");
        } catch (Exception e) {
            System.out.println(e);
        }

        var populacja = new Populacja(wielkoscPopulacji, k.wymiar);

        populacja.evaluate(k.odleglosci);
        populacja.keepTheBest();
        populacja.najlepszy.WyswietlGeny();

        for (int i = 1; i <= maxGeneracji; i++) {
            populacja.select();
            populacja.crossover(pKrzyzowania, k.wymiar);
            populacja.mutate(pMutacji, k.wymiar);
            populacja.evaluate(k.odleglosci);
            populacja.elitist();
            if (IF_DEBUG)
                populacja.PrintFitnessNajlepszego(i);
        }

        populacja.najlepszy.WyswietlGeny();
        populacja.najlepszy.WyswietlTrase();

        return (int)populacja.najlepszy.fitness;
    }
}
