package ewolucyjny;

public class Populacja {
    Osobnik[] populacja;
    Osobnik najlepszy;

    public Populacja(int ilosc, int geny) {
        populacja = new Osobnik[ilosc];
        for (int i = 0; i < ilosc; i++) {
            populacja[i] = new Osobnik(geny);
            populacja[i].generujGeny();
        }
    }

    void evaluate() {
        for (Osobnik os : populacja) {
            os.policzFitness();
        }
    }

    void keepTheBest() {
        for (Osobnik os : populacja) {
            if (najlepszy == null || najlepszy.fitness < os.fitness)
                najlepszy = os;
        }
    }

    void select() {
        obliczFitness();

        var nowaPopulacja = new Osobnik[populacja.length];

        for (int i = 0; i < populacja.length; i++) {
            var p = Math.random();
            if (p < populacja[0].cfitness)
                nowaPopulacja[i] = populacja[0];
            else {
                for (int j = 0; j < populacja.length; j++) {
                    var os = najlepszy;
                    if (j + 1 != populacja.length) {
                        os = populacja[j + 1];
                    }
                    if (p >= populacja[j].cfitness && p < os.cfitness)
                        nowaPopulacja[i] = os;
                }
            }
        }

        if (nowaPopulacja[0] != null)
            this.populacja = nowaPopulacja;
    }

    void crossover(double pKrzyzowania, int geny) {
        int first = 0;
        int one = 0;
        for (int mem = 0; mem < populacja.length; ++mem) {
            var x = Math.random();
            if (x < pKrzyzowania) {
                ++first;
                if (first % 2 == 0)
                    Xover(one, mem, geny);
                else
                    one = mem;
            }
        }
    }

    void mutate(double pMutacji, int geny) {
        for (int i = 0; i < populacja.length; i++)
            for (int j = 0; j < geny; j++) {
                var x = Math.random();
                if (x < pMutacji) {
                    populacja[i].genes[j] += losowa(-0.5, 0.5);
                    if (populacja[i].genes[j] < 0)
                        populacja[i].genes[j] = 0;
                    else if (populacja[i].genes[j] > Math.PI)
                        populacja[i].genes[j] = Math.PI;
                }
            }
    }

    void elitist() {
        var best = populacja[0].fitness;
        var worst = populacja[0].fitness;
        int best_idx = 0, worst_idx = 0;
        for (int i = 1; i < populacja.length; i++) {
            if (populacja[i].fitness >= best) {
                best = populacja[i].fitness;
                best_idx = i;
            }
            if (populacja[i].fitness < worst) {
                worst = populacja[i].fitness;
                worst_idx = i;
            }
        }

        if (best >= najlepszy.fitness) {
            for (int i = 0; i < najlepszy.genes.length; i++) {
                najlepszy.genes[i] = populacja[best_idx].genes[i];
            }
            najlepszy.fitness = populacja[best_idx].fitness;
        } else {
            for (int i = 0; i < najlepszy.genes.length; i++) {
                populacja[worst_idx].genes[i] = najlepszy.genes[i];
            }
            populacja[worst_idx].fitness = najlepszy.fitness;
        }
    }

    private void Xover(int one, int two, int geny) {
        // int point;
        if (geny <= 1)
            return;

        // if (geny == 2)
        // point = 1;
        // else
        // point = (int) (Math.random() * 1000) % (geny - 1) + 1;

        var alfa = Math.random();

        var tmp1 = new double[geny];
        var tmp2 = new double[geny];
        for (int i = 0; i < geny; i++) {
            tmp1[i] = alfa * populacja[one].genes[i] + (1 - alfa) * populacja[two].genes[i];
            tmp2[i] = (1 - alfa) * populacja[one].genes[i] + alfa * populacja[two].genes[i];
        }

        for (int i = 0; i < geny; i++) {
            populacja[one].genes[i] = tmp1[i];
            populacja[two].genes[i] = tmp2[i];
            // var tmp = populacja[one].genes[i];
            // populacja[one].genes[i] = populacja[two].genes[i];
            // populacja[two].genes[i] = tmp;
        }
    }

    public static double losowa(double min, double max) {
        return min + Math.random() * (max - min);
    }

    void PrintFitnessNajlepszego(int gen) {
        System.out.println("Generation " + gen + " fitnness " + najlepszy.fitness);
    }

    private void obliczFitness() {
        var sum = 0.0;
        for (Osobnik os : populacja) {
            sum += os.fitness;
        }

        for (Osobnik os : populacja) {
            os.rfitenss = os.fitness / sum;
        }

        populacja[0].cfitness = populacja[0].rfitenss;
        for (int i = 1; i < populacja.length; i++) {
            populacja[i].cfitness = populacja[i - 1].cfitness + populacja[i].rfitenss;
        }
    }
}
