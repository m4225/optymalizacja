package ewolucyjny;

public class Osobnik {
    double[] genes;
    double fitness;
    double rfitenss;
    double cfitness;
    int n;

    public Osobnik(int n) {
        genes = new double[n];
        for (int i = 0; i < n; i++)
            genes[i] = 0;

        this.n = n;
        fitness = 0;
        rfitenss = 0;
        cfitness = 0;
    }

    public static double losowa(double min, double max) {
        return min + Math.random() * (max - min);
    }

    void generujGeny() {
        for (int i = 0; i < n; i++)
            genes[i] = losowa(0, Math.PI);
    }

    double policzFitness() {
        fitness = 1000.0;
        for (double gene : genes) {
            fitness *= Math.sin(gene);
        }

        return fitness;
    }

    public void WyswietlGeny() {
        // if (!WspinaczkowyKomiwojazer.IF_DEBUG){
        //     return;
        // }

        System.out.print("Geny:\t");
        for (double t : genes) {
            System.out.print(t + " ");
        }
        System.out.println();
    }
}
