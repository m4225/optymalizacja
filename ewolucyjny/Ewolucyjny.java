package ewolucyjny;

public class Ewolucyjny {
    public static void main(String[] args) {
        var iloscGenow = 5;
        var wielkoscPopulacji = 25;
        var maxGeneracji = 1000;
        var pKrzyzowania = 0.8;
        var pMutacji = 0.2;

        var populacja = new Populacja(wielkoscPopulacji, iloscGenow);

        populacja.evaluate();
        populacja.keepTheBest();
        populacja.najlepszy.WyswietlGeny();

        for (int i = 1; i < maxGeneracji; i++) {
            populacja.select();
            populacja.crossover(pKrzyzowania, iloscGenow);
            populacja.mutate(pMutacji, iloscGenow);
            populacja.evaluate();
            populacja.elitist();
            populacja.PrintFitnessNajlepszego(i);
        }

        populacja.najlepszy.WyswietlGeny();
    }
}
