package ewolucyjnyPlecak;

public class Osobnik {
    public int wymiar;
    int[] genes, kosz, plecak;
    double fitness, cfitness, rfitenss, maxWaga;

    public Osobnik(Osobnik os) {
        this.wymiar = os.wymiar;
        this.genes = makeCopy(os.genes);
        this.kosz = makeCopy(os.kosz);
        this.plecak = makeCopy(os.plecak);
        this.fitness = os.fitness;
        this.cfitness = os.cfitness;
        this.rfitenss = os.rfitenss;
        this.maxWaga = os.maxWaga;
    }

    //Tworzy kopię tablicy
    private static int[] makeCopy(int[] arr) {
        var wynik = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            wynik[i] = arr[i];
        }

        return wynik;
    }

    public Osobnik(int wymiar, double maxWaga) {
        super();
        this.wymiar = wymiar;
        this.maxWaga = maxWaga;
        genes = new int[wymiar];
        kosz = new int[wymiar];
        plecak = new int[wymiar];
    }

    // inicjalizuje tablicę genów, tablicę kosza i na koniec generuje z nich tablicę plecaka
    // geny są losowane w kolejnej iteracji z przedziału <0, i> gdzie i zaczyna się od długości tablicy i spada do 0
    // kosz jest kolejnymi wartościami i malejąco np. [i, i-1, ..., 2, 1, 0]
    public void Inicjalizuj() {
        var lastIdx = wymiar - 1;
        for (int i = lastIdx; i > 0; i--) {
            genes[lastIdx - i] = (int) (Math.random() * 1000) % i;
            kosz[lastIdx - i] = i;
        }

        genes[lastIdx] = 0;
        kosz[lastIdx] = 0;
        this.UstalPlecak();
    }

    // ustala kolejnosc elementow w plecaku według ustawionych genów i kosza
    public void UstalPlecak() {
        var temp_kosz = new int[kosz.length];
        for (int i = 0; i < kosz.length; i++)
            temp_kosz[i] = kosz[i];
        for (int i = 0; i < wymiar; i++) {
            plecak[i] = kosz[genes[i]];
            usunZKosza(genes[i]);
        }
        for (int i = 0; i < kosz.length; i++)
            kosz[i] = temp_kosz[i];
    }

    //usuwa element pod wybranymi indeksem z kosza
    void usunZKosza(int poz) {
        for (int i = poz; i < wymiar - 1; i++) {
            kosz[i] = kosz[i + 1];
        }
    }

    //liczy fitness gdzie fitness jest sumą cen elementów plecaka dopóki suma wag elementów nie przekroczy maxWagi
    double policzFitness(double[] ceny, double[] wagi) {
        fitness = 0;
        var waga = 0.0;
        for (int idx : plecak) {
            waga += wagi[idx];
            if (waga > maxWaga) {
                break;
            }
            fitness += ceny[idx];
        }

        return fitness;
    }

    public void WyswietlPlecak(double[] ceny, double[] wagi) {
        if (!EwolucyjnyPlecak.IF_DEBUG) {
            return;
        }

        System.out.print("Plecak:\t");
        var waga = 0.0;
        for (int t : plecak) {
            waga += wagi[t];
            if (waga > maxWaga) {
                waga-=wagi[t];
                break;
            }

            System.out.print(t + ": {" + ceny[t] + " " + wagi[t] + "} ");
        }
        System.out.println("Waga: "+waga+"\n");
    }

    public void WyswietlGeny() {
        if (!EwolucyjnyPlecak.IF_DEBUG) {
            return;
        }

        System.out.print("Geny:\t");
        for (int t : genes) {
            System.out.print(t + " ");
        }
        System.out.println();
    }

    public void WyswietlDopasowanie() {
        System.out.println("Dopasowanie: " + fitness);
    }
}
