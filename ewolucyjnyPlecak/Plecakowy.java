package ewolucyjnyPlecak;

import java.io.File;
import java.util.*;

public class Plecakowy {
    public int[] identyfikatory;
    public double[] wagi, ceny;
    public int wymiar;

    void Wczytaj(String nazwa) throws Exception {
        File f = new File(nazwa);
        Scanner in = new Scanner(f);

        List<Double> cs = new ArrayList<Double>();
        List<Double> ws = new ArrayList<Double>();
        while (in.hasNextInt()) {
            var cena = in.nextDouble();
            var waga = in.nextDouble();
            cs.add(cena);
            ws.add(waga);
        }

        in.close();

        wymiar = cs.size();
        identyfikatory = new int[wymiar];
        ceny = new double[wymiar];
        wagi = new double[wymiar];

        for (int i = 0; i < wymiar; i++) {
            identyfikatory[i] = i;
            ceny[i] = cs.get(i);
            wagi[i] = ws.get(i);
        }
    }
}
