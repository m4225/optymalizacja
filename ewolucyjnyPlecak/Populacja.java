package ewolucyjnyPlecak;

public class Populacja {
    Osobnik[] populacja;
    Osobnik najlepszy;

    //generuje nową populację
    public Populacja(int ilosc, int geny, double maxWaga) {
        populacja = new Osobnik[ilosc];
        for (int i = 0; i < ilosc; i++) {
            populacja[i] = new Osobnik(geny, maxWaga);
            populacja[i].Inicjalizuj();
        }
    }

    //ustawia kolejność elemetów w plecaku każdego osobnika i generuj dla nich fitness
    void evaluate(double[] ceny, double[] wagi) {
        for (Osobnik os : populacja) {
            os.UstalPlecak();
            os.policzFitness(ceny, wagi);
        }
    }

    //wybiera najlepszego osobnika z populacji
    void keepTheBest() {
        for (Osobnik os : populacja) {
            if (najlepszy == null || najlepszy.fitness < os.fitness)
                najlepszy = os;
        }
    }

    //wybiera nową populację osobników
    void select() {
        obliczFitness();

        var nowaPopulacja = new Osobnik[populacja.length];

        for (int i = 0; i < populacja.length; i++) {
            var p = Math.random();
            if (p < populacja[0].cfitness)
                nowaPopulacja[i] = populacja[0];
            else {
                for (int j = 0; j < populacja.length; j++) {
                    var os = najlepszy;
                    if (j + 1 != populacja.length) {
                        os = populacja[j + 1];
                    }
                    if (p >= populacja[j].cfitness && p < os.cfitness)
                        nowaPopulacja[i] = new Osobnik(os);
                }
            }
        }

        // if (nowaPopulacja[0] != null
        this.populacja = nowaPopulacja;
    }

    // krzyżuje osobniki
    void crossover(double pKrzyzowania, int geny) {
        int first = 0;
        int one = 0;
        for (int mem = 0; mem < populacja.length; ++mem) {
            var x = Math.random();
            if (x < pKrzyzowania) {
                ++first;
                if (first % 2 == 0)
                    Xover(one, mem, geny);
                else
                    one = mem;
            }
        }
    }

    //mutuje osobnika
    void mutate(double pMutacji, int geny) {
        for (int i = 0; i < populacja.length; i++)
            for (int j = 0; j < geny; j++) {
                var x = Math.random();
                if (x < pMutacji) {
                    populacja[i].genes[j] = (int) (Math.random() * 1000) % (geny - j);
                }
            }
    }

    //wybiera najlepszego i najgorszego osobnika z obecnej populacji
    //jeżeli najlepszy z tej populacji jest lepszy od poprzedniego to zmienia go na nowego
    //jeżeli jest gorszy to najgorszy osobnik jest zmieniany na najlepszego z poprzedniej populacji
    void elitist() {
        var best = populacja[0].fitness;
        var worst = populacja[0].fitness;
        int best_idx = 0, worst_idx = 0;
        for (int i = 1; i < populacja.length; i++) {
            if (populacja[i].fitness >= best) {
                best = populacja[i].fitness;
                best_idx = i;
            }
            if (populacja[i].fitness < worst) {
                worst = populacja[i].fitness;
                worst_idx = i;
            }
        }

        if (best >= najlepszy.fitness) {
            najlepszy = new Osobnik(populacja[best_idx]);
        } else {
            populacja[worst_idx] = new Osobnik(najlepszy);
        }
    }

    // krzyżuje 2 osobniki
    private void Xover(int one, int two, int geny) {
        int point;
        if (geny <= 1)
            return;

        if (geny == 2)
            point = 1;
        else
            point = (int) (Math.random() * 1000) % (geny - 1) + 1;

        for (int i = 0; i < point; i++) {
            var tmp = populacja[one].genes[i];
            populacja[one].genes[i] = populacja[two].genes[i];
            populacja[two].genes[i] = tmp;
        }
    }

    public static double losowa(double min, double max) {
        return min + Math.random() * (max - min);
    }

    void PrintFitnessNajlepszego(int gen) {
        System.out.println("Generation " + gen + " fitness " + najlepszy.fitness);
    }

    //liczy dopasowania populacji
    private void obliczFitness() {
        var sum = 0.0;
        for (Osobnik os : populacja) {
            sum += os.fitness;
        }

        for (Osobnik os : populacja) {
            os.rfitenss = os.fitness / sum;
        }

        populacja[0].cfitness = populacja[0].rfitenss;
        for (int i = 1; i < populacja.length; i++) {
            populacja[i].cfitness = populacja[i - 1].cfitness + populacja[i].rfitenss;
        }
    }
}
