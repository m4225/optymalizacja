package ewolucyjnyPlecak;

public class EwolucyjnyPlecak {
    public static boolean IF_DEBUG = true;

    public static void main(String[] args) {
        var wielkoscPopulacji = 25;
        var maxGeneracji = 1000;
        var pKrzyzowania = 0.8;
        var pMutacji = 0.1;
        var maxWaga = 64.0;

        var k = new Plecakowy();
        try {
            k.Wczytaj("przedmioty.txt");
        } catch (Exception e) {
            System.out.println(e);
        }

        var populacja = new Populacja(wielkoscPopulacji, k.wymiar, maxWaga);

        populacja.evaluate(k.ceny, k.wagi);
        populacja.keepTheBest();
        populacja.najlepszy.WyswietlGeny();

        for (int i = 1; i <= maxGeneracji; i++) {
            populacja.select();
            populacja.crossover(pKrzyzowania, k.wymiar);
            populacja.mutate(pMutacji, k.wymiar);
            populacja.evaluate(k.ceny, k.wagi);
            populacja.elitist();
            if (IF_DEBUG)
                populacja.PrintFitnessNajlepszego(i);

            // populacja.najlepszy.WyswietlPlecak(k.ceny, k.wagi);
        }

        populacja.najlepszy.WyswietlGeny();
        populacja.najlepszy.UstalPlecak();
        populacja.najlepszy.WyswietlPlecak(k.ceny, k.wagi);
        populacja.najlepszy.WyswietlDopasowanie();

    }
}
